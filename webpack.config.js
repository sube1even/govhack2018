const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: './app/index.js',
  mode: 'production',
  devtool: 'inline-source-map',
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, '/dist'),
  },
  module: {
    rules: [{
      test: /\.html$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            interpolate: true
          }
        },
        { loader: 'extract-loader' },
        {
          loader: 'html-loader',
          options: {
            interpolate: true,
            minimize: true,
            removeComments: true,
            conservativeCollapse: true,
            collapseWhitespace: true,
          }
        }
      ]
    },
    {
      test: /\.(png|gif|jpg)$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            name: 'images/[name].[ext]',
            limit: 8192
          }
        },
        {
          loader: 'image-webpack-loader',
          options: {
            mozjpeg: {
              progressive: true,
              quality: 65
            },
            optipng: {
              enabled: true,
            },
            pngquant: {
              quality: '65-90',
              speed: 4
            },
            gifsicle: {
              interlaced: false,
            }
          }
        }
      ]
    },
    {
      test: /\.(woff(2)?|ttf|eot|svg)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]'
          }
        }
      ]
    },
    {
      test: /\.scss$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[name].css'
          }
        },
        { loader: 'extract-loader' },
        //{ loader: 'style-loader' },
        { loader: 'css-loader' },
        { loader: 'sass-loader' }
      ]
    }]
  },
  devServer: {
    contentBase: './dist'
  },
  plugins: [
    new CleanWebpackPlugin(['fonts', 'images']),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    })
  ]
}

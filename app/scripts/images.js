var Images = (function($, config) {
	var size = config.imageFeedSize;
	var page = 0;
	var staticImages = 
		{images: [
			{url: 'images/entry-1.jpg'},
			{url: 'images/entry-2.jpg'},
			{url: 'images/entry-3.jpg'},
			{url: 'images/entry-4.jpg'},
			{url: 'images/entry-5.jpg'},
		]};
	function fetchImages() {	
		return new Promise(function(resolve, reject) {
			// Ajax fetch image feed data, if fails return mocked list
			$.ajax({
				url: config.domain + '/api/imagefeed?page=' + page + '&size=' + size,
				dataType: 'JSON',
				asynx: true,
				cache: true,
				success: function(data) {
					if(!data.images || data.images.lenth === 0) {
						resolve(staticImages);
					}
					else {
						if(data.images.length === size) {
							page++;
						}
						
						resolve(data);
					}
				},
				error: function(err) {
					// Failed to retrieve images...
					// Return hard coded list of images
					
					resolve(staticImages);
				},
			});
		});
	}
	
	return {
		fetchImages: fetchImages
	}
}(jQuery, Config));
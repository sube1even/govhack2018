module.exports = Map = (function () {
  var coopersmapcenter = new google.maps.LatLng(-34.9198844, 138.7370085);
  var markericon = require('../images/icons-location.png');
  var map;
  var mapElement;
  var markers = [];
  var infoWindows = [];

  function init(mapElementName, venues) {
    var mapOptions = {
      center: coopersmapcenter,
      zoom: 11,
      maxZoom: 16,
      minZoom: 5,
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.SMALL,
      },
      disableDoubleClickZoom: true,
      mapTypeControl: false,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
      },
      scaleControl: true,
      scrollwheel: true,
      panControl: false,
      streetViewControl: false,
      draggable: true,
      overviewMapControl: false,
      overviewMapControlOptions: {
        opened: false,
      },
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: getMapStyles()
    }

    var bounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(-43.388870, 111.165326),
      new google.maps.LatLng(-9.795391, 155.594036)
      );

    mapElement = document.getElementById(mapElementName);
    map = new google.maps.Map(mapElement, mapOptions);

    // Add map event listeners
    google.maps.event.addListener(map, 'bounds_changed', function () {
      if (bounds.contains(map.getCenter())) return;

      // We're out of bounds - Move the map back within the bounds
      var c = map.getCenter(),
        x = c.lng(),
        y = c.lat(),
        maxX = bounds.getNorthEast().lng(),
        maxY = bounds.getNorthEast().lat(),
        minX = bounds.getSouthWest().lng(),
        minY = bounds.getSouthWest().lat();

      if (x < minX) x = minX;
      if (x > maxX) x = maxX;
      if (y < minY) y = minY;
      if (y > maxY) y = maxY;

      map.setCenter(new google.maps.LatLng(y, x));
    });

    addLocationPins(venues);
  }

  // Add map pins
  function addLocationPins(venues) {
    $.each(venues, function (key, venue) {
      // Add a pin for each venue
      var markerPos = new google.maps.LatLng(venue.latitude, venue.logitude);
      var marker = new google.maps.Marker({
        icon: markericon,
        position: markerPos,
        map: map,
        title: venue.name,
        desc: venue.address
      });

      // Add info window for marker
      bindInfoWindow(marker, map, venue);
      marker.venueId = venue.id;
      markers.push(marker);
    });
  }

  // Create an info window for a venue marker
  function bindInfoWindow(marker, map, venue) {
    var infoWindowVisible = (function () {
      var currentlyVisible = false;
      return function (visible) {
        if (visible !== undefined) {
          currentlyVisible = visible;
        }
        return currentlyVisible;
      };
    } ());

    var html =
    "<div style='color:#000;padding:0 15px 15px;font-family: arial;'>" +
      '<h4><span class="icon-location"></span>&nbsp;'+venue.name+'</h4>\n' +
        venue.address + ', ' + venue.state + ' ' + venue.postcode + '\n' +
        (venue.phone ? '<br> Ph: '+venue.phone+'\n' : '')+
      "</div>";

    marker.infoWindow = new google.maps.InfoWindow({
          content: html
        });

    google.maps.event.addListener(marker, 'click', function () {
      if (infoWindowVisible()) {
        marker.infoWindow.close();
        infoWindowVisible(false);
        marker.setAnimation(null);
      } else {
        $.each(markers, function(k, v) {
          v.infoWindow.close();
          v.setAnimation(null);
        });
        marker.infoWindow.open(map, marker);
        infoWindowVisible(true);
        animateMarker(marker);
      }
    });

    google.maps.event.addListener(marker.infoWindow, 'closeclick', function () {
      infoWindowVisible(false);
    });
  }

  function zoomToLocation(lat, long, venueId) {
    var location = new google.maps.LatLng(lat, long);
    map.setZoom(15);
    map.panTo(location);
    $.each(markers, function(k, v) {
      v.infoWindow.close();
      v.setAnimation(null);
    });
    $.each(markers, function(key, marker) {
      if(marker.venueId == venueId) {
        animateMarker(marker)
      }
    })
  }

  function animateMarker(marker) {
    marker.setAnimation(google.maps.Animation.BOUNCE);
    setTimeout(function () {
      marker.setAnimation(null);
    }, 3500);
  }

  function getMap() {
    return map;
  }

  function getMapStyles() {
    return [{
      "featureType": "all",
      "elementType": "labels.text.fill",
      "stylers": [{
        "saturation": 36
      }, {
          "color": "#000000"
        }, {
          "lightness": 40
        }]
    }, {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [{
          "visibility": "on"
        }, {
            "color": "#000000"
          }, {
            "lightness": 16
          }]
      }, {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [{
          "visibility": "off"
        }]
      }, {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 20
          }]
      }, {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 17
          }, {
            "weight": 1.2
          }]
      }, {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 20
          }]
      }, {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 21
          }]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 17
          }]
      }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 29
          }, {
            "weight": 0.2
          }]
      }, {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 18
          }]
      }, {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 16
          }]
      }, {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 19
          }]
      }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [{
          "color": "#000000"
        }, {
            "lightness": 17
          }]
      }]
  }

  return {
    init: init,
    getMap: getMap,
    zoomToLocation: zoomToLocation,
  };
} ());

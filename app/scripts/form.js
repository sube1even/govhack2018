const Config = require('./config');
module.exports = Forms = (function($, config) {

	/**
	 * Initialise venues module, triggers ajax retrieval of data
	 */
	function init() {

		function resizsect(){
			var step = "#" + $('.section.entryform').attr('data-step');
			var headerh = $('.entryform-inner .header').outerHeight(true);
			var h = $(step).outerHeight(true);
			$('.section.entryform').height(h+headerh);
		}
		resizsect();

    $(window).resize(function() {
      resizsect();
    })

  // Cancel entry and reset form to initial state
  $('.cancel-entry').on('click', function(e) {
    $('.section.entryform').attr('data-step','entry-step-one').removeClass('entry-step-two').removeClass('entry-step-three');
    $('#entry-step-one').removeAttr('style');
    $('#entry-step-two').removeAttr('style');
    $('.section.entryform').removeAttr('style');
		resizsect();
    e.preventDefault();
  });


  $('#entryCode').on('focus',function() {
    $('#entry-step-one .error-text').text('').removeClass('visible');
    $('#entry-step-one #entryCode').removeClass('error');
  })

  $('#submitCode').on('click', function(e) {
    // clear errors
      $('#entry-step-one .error-text').text('').removeClass('visible');
      $('#entry-step-one #entryCode').removeClass('error');

      ga('send', 'event', 'Entries', 'code');
      var entryCode = $('#entryCode').val();

      $.post(config.domain+'/api/entries/code', '='+entryCode)
          .done(function(data) {
              if (data) {
                  console.log('SUCCESS Step 1');
                  $('#entryDetailsCode,#entryDetailsCode2').val($('#entryCode').val());
                  var h = $('#entry-step-one').outerHeight(true);
                  $('.section.entryform').attr('data-step','entry-step-two').addClass('entry-step-two');
									resizsect();
                  $('#entry-step-one').css('top',-h+'px')
              } else {
                //alert('Invalid code. Please try again.');
                $('#entry-step-one #entryCode').addClass('error');
                $('#entry-step-one .error-text').text('Invalid code. Please try again').addClass('visible');
              }

          })
          .fail(function() {
            //alert('Please enter a code.');
            $('#entry-step-one #entryCode').addClass('error');
            $('#entry-step-one .error-text').text('Please enter a code').addClass('visible');
          });
  });

  $('#btnEntrySubmit').on('click', function(e) {
    $('#entry-step-two #name, #entry-step-two #email, #entry-step-two #phoneNumber').removeClass('error');
    $('#entry-step-two .error-text').text('').removeClass('visible');
      e.preventDefault();
      ga('send', 'event', 'Entries', 'entry');
      $.post(config.domain+'/api/entries/details', $('#entryDetails').serialize())
          .done(function(data) {
              if (data && data > 0) {

                  console.log('SUCCESS Step 2');
                  $('#entryId').val(data);
                  var h = $('#entry-step-two').outerHeight(true);
                  $('.section.entryform').attr('data-step','entry-step-three').removeClass('entry-step-two').addClass('entry-step-three');
									resizsect();
                  $('#entry-step-two').css('top',-h+'px');
              } else {
                console.log('ERROR Step 2', data);
                //alert('All fields are required. Please ensure you select a venue from list options.');
                $('#entry-step-two #name, #entry-step-two #email, #entry-step-two #phoneNumber, #entry-step-two #venue-typeahead input').addClass('error');
                $('#entry-step-two .error-text').text('All fields are required. Please ensure you select a venue from list options').addClass('visible');
              }
          })
          .fail(function() {
            $('#entry-step-two .error-text').text('Network connection lost reload page and try again').addClass('visible');
          });
  });

  // $('#btnImageSubmit').on('click', function(e) {
  //   ga('send', 'event', 'Entries', 'image');
  //     e.preventDefault();
  //     var formData = new FormData();
  //     var opmlFile = $('#entryImage')[0];
  //     formData.append("entryImage", opmlFile.files[0]);
  //     formData.append("entryId", $('#entryId').val());

  //     $.ajax({
  //         url: config.domain+'/api/entries/upload',
  //         type: 'POST',
  //         data: formData,
  //         cache: false,
  //         contentType: false,
  //         processData: false,
  //         success: function(data) {
  //             console.log('SUCCESS Step 3', data);
  //             //data returned is image src url
  //             var entryImg = "<img src='"+data+"' class='entry-img-uploaded'>";
  //             $('#stepThreeImage').append(entryImg);
  //         },
  //         error: function(data) {
  //             console.log('ERROR', data);
  //         }
  //     });
  // });

}
return {
  init: init
}
}(jQuery, Config));

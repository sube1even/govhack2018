const Config = require('./config');

module.exports = Venues = (function($, config) {
	var venues = [];
	/**
	 * Initialise venues module, triggers ajax retrieval of data
	 */
	function init() {
		var p = Promise.resolve($.ajax({
			url: config.domain + '/api/venues/list',
			dataType: 'JSON',
			asynx: true,
			cache: true,
			success: function(data) {
				venues = data;
			},
			error: function() {
				console.log('Failed to retrive Venues.');
			}
		}));
		
		return p;
	}
	
	/**
	 * Returns object of all venues in a key value pair
	 * to populate a select list
	 */
	function getVenueOptions() {
		// format venue.id: '{venue.name}, {venue.suburb}, {venue.state}'
		var options = {};
		$.each(venues, function(index, value) {
			options[value.id] = value.name + ", " + value.suburb + ", " + value.state;
		});
		return options;
	}
	
	/**
	 * Returns array of venues to iterate over and bind to front end
	 */
	function getVenueList() {
		return venues;
	}
	
	/**
	 * Returns a filtered array of venues
	 * Searched venue suburb and postcode using filter param
	 */
	function getFilteredVenues(filter) {
		if(filter === '') {
			return [];
		}
		var re = new RegExp(filter, "i"); // "i" case-insensitive
		return venues.filter(function(v) {
			return re.test(v.postcode);
		});
	}
	
	/**
	 * Query server for nearby venues based on postcode search
	 * Returns an array of venue Id's that are located nearby
	 */
	function findNearbyVenues(postcode) {
		return new Promise(function(resolve, reject) {
			$.ajax({
				url: config.domain + '/api/venues/postcodesearch?postcode=' + postcode + '&distance=10000',
				dataType: 'JSON',
				asynx: true,
				cache: true,
				success: function(data) {
					resolve(venues.filter(function(v) {
						return $.inArray(v.id, data) != -1;
					}));
				},
				error: function(err) {
					reject(err);
				}
			});
		});
	}
	
	
	return {
		init: init,
		getVenueOptions: getVenueOptions,
		getVenueList: getVenueList,
		getFilteredVenues: getFilteredVenues,
		findNearbyVenues: findNearbyVenues
	}
}(jQuery, Config));

module.exports = Config = (function() {
	return {
		// Empty domain value will call services against apps current domain
		// Populate this value to call from a different domain
		domain: '',
		imageFeedSize: 12
	}
}());

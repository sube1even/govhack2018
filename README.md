# HTML Boilerplate using Webpack

This project is intended for flat HTML development using webpack to hot reload and compile SCSS and JS.

## Requirements

- [NPM](https://www.npmjs.com/)

## Get started

From the project root:

`npm install`
`npm start`

Access [localhost:8081](localhost:8081) from your browser.

## Docs

- [Webpack](https://webpack.js.org/)

